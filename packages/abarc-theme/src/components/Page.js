import React, {useEffect} from "react"
import { connect } from "frontity"

//functions 
import { setLanguageInfos } from "./functions/setLanguageInfos"

const Page = ({ state, libraries }) => {
  const data = state.source.get(state.router.link)
  const page = state.source[data.type][data.id]
  const Html2React = libraries.html2react.Component
  const dataPosts = state.source.get('/')
  console.log('PAGE',page)
  

  useEffect(() => {
    /// SET RELATIVE PATH
    setLanguageInfos(page, state.theme)
  },[])

  return (
    <div>
      PAGE
       <h2>{page.title.rendered}</h2>
        <Html2React html={page.content.rendered} />
    </div>
  )
}

export default connect(Page)