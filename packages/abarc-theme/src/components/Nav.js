import React from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"


// WP-REST-API V2 Menus PLUGIN IS REQUIRED!

/**
 * Two level menu (with one level of child menus)
 */
const Nav = ({ state }) => {
  console.log('LANGYAGE',state.lang)
  const items = state.source.get(`${state.lang === 'en' ? '/en' : ''}/menu/${state.lang === 'it' ? state.theme.rapidNavIta : state.theme.rapidNavEng}/`).items
  console.log('ITEMS:',items)
  return (
    <nav>
      {items && items.map(item => {
        return(
          <li
            key={item.ID}
          >
            <Link link={`/${state.lang ==='it' ? '' : 'en/'}${item.slug}`}>
              {item.title}
            </Link>
          </li>
        )
      })}
    </nav>
  );
} 


export default connect(Nav);
