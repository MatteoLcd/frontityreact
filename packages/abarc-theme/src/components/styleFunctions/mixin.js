export const pxToRem = (fontSize,globalSize) => {
    return fontSize/globalSize+'rem';
}

export const childCol = (parentCol, childCol) => {
    let col1 = 8.3;
    const colonne = [
        {
            1:col1,
            2:col1*2,
            3:col1*3,
            4:col1*4,
            5:col1*5,
            6:col1*6,
            7:col1*7,
            8:col1*8,
            9:col1*9,
            10:col1*10,
            11:col1*11,
            12:100
        }
    ]
    let proporzioneEstremi = colonne[0][childCol] * 100;
    let proporzioneX = proporzioneEstremi / colonne[0][parentCol];
    let xPercent = proporzioneX * 1+'%';
    return xPercent;
}
