import React, {useEffect} from 'react';
import { connect } from 'frontity';

//Components 
import List from '../List';

const PostTypeArchive = ({state}) => {
    const data = state.source.get(state.router.link)
    const postTypes = state.source.postTypes

    const setRelativePath = () => {
        if(data.route === '/news/' || data.route === '/en/news/') {
            state.theme.relativePathEng='/en/news'
            state.theme.relativePathIta='/news'
            if(state.lang === 'en') {
            state.theme.isInIta=true
            state.theme.isInEng=false
            } else {
            state.theme.isInIta=false
            state.theme.isInEng=true
            }
        } else {
            postTypes && postTypes.map(postType => {
                if(data.route === `/${postType.endpoint}/` || data.route === `/en/${postType.endpoint}/`) {
                    state.theme.relativePathEng=`/en/${postType.endpoint}`
                    state.theme.relativePathIta=`/${postType.endpoint}`
                    if(state.lang === 'en') {
                    state.theme.isInIta=true
                    state.theme.isInEng=false
                    } else {
                    state.theme.isInIta=false
                    state.theme.isInEng=true
                    }
                }
            })
        }
    }

    useEffect(() => {
        // console.log('SET RELATIVE ARCHIVE')
        setRelativePath()
    },[])

    useEffect(() => {
        /// SET RELATIVE PATH
        setRelativePath()
    },[state.lang])

    return (
        <div>
            PostTypeArchive
            <List/>
        </div>
    )
}

export default connect(PostTypeArchive)