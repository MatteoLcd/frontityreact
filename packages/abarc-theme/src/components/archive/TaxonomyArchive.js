import React, {useEffect} from 'react';
import { connect } from 'frontity';

//Components 
import List from '../List';

const TaxonomyArchive = ({state}) => {
    const data = state.source.get(state.router.link)
    const taxonomy = state.source[data.taxonomy][data.id]

    const setRelativePath = () => {
        if(taxonomy['key'] !== null) {
            state.theme.relativePathEng=`${taxonomy['taxonomy_translate']['path']}`
            state.theme.relativePathIta=`${taxonomy['taxonomy_translate']['path']}`

          if(state.lang === 'en') {
            state.theme.isInIta=true
            state.theme.isInEng=false
          } else {
            state.theme.isInIta=false
            state.theme.isInEng=true
          }
        } else {
            state.theme.isInIta=false
            state.theme.isInEng=false
        }
    }

    useEffect(() => {
        // console.log('SET RELATIVE ARCHIVE')
        setRelativePath()
    },[])

    useEffect(() => {
        /// SET RELATIVE PATH
        setRelativePath()
    },[state.lang])

    return (
        <div>
            TaxonomyArchive
            <List/>
        </div>
    )
}

export default connect(TaxonomyArchive)