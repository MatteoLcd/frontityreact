
import React, {useEffect, useState} from "react"
import { connect } from "frontity"
import Link from "@frontity/components/link"

const List = ({ state, actions }) => {
  const data = state.source.get(state.router.link)
  const dataItems = data.items;

  
 


  return (
    <>
      <div>

       {data.items.map((item) => {
          
              const post = state.source[item.type][item.id]
              return (
                <Link 
                  key={item.id} 
                  link={post.link }>
                  {post.title.rendered}
                  <br />
                </Link>
              )
            })}
            {
                data.previous && (
                    <button
                      onClick={() => {
                          actions.router.set(data.previous)
                      }}
                    >Prev</button>
                )
            }
            {
                data.next && (
                    <button
                      onClick={() => {
                          actions.router.set(data.next)
                      }}
                    >Next</button>
                )
            } 
      </div>
    </>
  )
}

export default connect(List)