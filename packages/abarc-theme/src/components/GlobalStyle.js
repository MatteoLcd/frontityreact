import React from "react"
import { Global, css, createGlobalStyle } from 'frontity'
import { Display } from "./styles/Display"
import { GlobalTheme } from "./styles/Global"
import { Single } from "./styles/Single"
import { Visita } from "./styles/blocks/Visita"
import { Headings } from "./styles/Headings"
import { Copy } from "./styles/Copy"


const GlobalStyle = () => {

  return (
    <Global
      styles={[
          GlobalTheme, 
          Display,
          Single,
          Visita,
          Headings,
          Copy
        ]}
    />
  )
}

export default GlobalStyle