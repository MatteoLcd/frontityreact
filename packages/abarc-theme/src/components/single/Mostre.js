import React, {useEffect} from 'react';
import { connect } from 'frontity';
import dayjs from "dayjs"

//functions
import { setLanguageInfos } from "./../functions/setLanguageInfos"

//Styled
import { Header, DateContainer } from './Mostre.styles';

const Mostre = ({state}) => {
    const data = state.source.get(state.router.link)
    const mostra = state.source[data.type][data.id]
    // const formattedDate = dayjs(mostra.date).format("DD MMMM YYYY")
    console.log('MOSTRA',mostra)

    useEffect(() => {
        /// SET RELATIVE PATH
        setLanguageInfos(mostra, state.theme)
    },[])

    return(
        <Header
            className='entry-header m900-container-medium'
        >
            <DateContainer
                className='mostra-date-container d-flex flex-wrap'
            >
                Data
            </DateContainer>
        </Header>
    )
}

export default connect(Mostre)