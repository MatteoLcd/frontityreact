import React, {useEffect} from "react"
import { connect, Global, css, Head } from 'frontity'
import Link from "@frontity/components/link"
import Switch from "@frontity/components/switch"


//components
import List from './List'
import Post from './Post'
import Page from './Page'
import GlobalStyle from "./GlobalStyle"
import Loading from './Loading';
import Error from './Error';
import Nav from "./Nav";
import PageForPosts from "./PageForPosts"
import LanguageSwitch from "./LanguageSwitch"
import IsArchive from './IsArchive'
import Mostre from "./single/Mostre"


const Root = ({state, actions, libraries}) => {
  const data = state.source.get(state.router.link)

  console.log('DATA',data)
  return (
    <div
      className={`${data.isSingle && 'single'} ${data.isMostre && 'single-mostre'}`}
    >
    <Head>
      <html lang="it"/>
      <link rel="preconnect" href="https://fonts.googleapis.com"/>
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Roboto+Slab:wght@400;500&display=swap" rel="stylesheet"></link>
    </Head>
    <GlobalStyle/>
    <Link
      link={state.lang === 'it' ? '/' : '/en/'}
    >
      <h1>{state.frontity.name}</h1>
    </Link>
      <button
       onClick={actions.theme.toggleUrl}
      >Toggle Url</button>
      {state.theme.isUrlVisible ? state.router.link : null}
      <LanguageSwitch/>
      <Nav/>
      <hr />
      <main className='primary site-main'>
        <Switch>
          <Loading when={data.isFetching}/>
          <div when={data.isArchive}><IsArchive/></div>
          <div when={data.isPost || data.isDidattica}><Post/></div>
          <div when={data.isPage}><Page/></div>
          <Mostre when={data.isMostre}/>
          <Error when={data.isError}/>
          {/* <div when={data.isPage && currentElement.is_page_for_posts}><PageForPosts/></div> */}
        </Switch>
      </main>
    </div>
  )
}

export default connect(Root)