import React, {useEffect} from "react"
import { connect } from "frontity"
import dayjs from "dayjs"

//functions
import { setLanguageInfos } from "./functions/setLanguageInfos"

const Post = ({ state, libraries }) => {
  const data = state.source.get(state.router.link)
  const post = state.source[data.type][data.id]
  const formattedDate = dayjs(post.date).format("DD MMMM YYYY")
  const Html2React = libraries.html2react.Component

  console.log('DATAPOST',post)

  useEffect(() => {
    /// SET RELATIVE PATH
    setLanguageInfos(post, state.theme)
  },[])

  return (
    <div>
        <p>
            <strong>Posted: </strong>
            {formattedDate}
        </p>
        <h2>{post.title.rendered}</h2>
        <Html2React html={post.content.rendered} />
    </div>
  )
}

export default connect(Post)