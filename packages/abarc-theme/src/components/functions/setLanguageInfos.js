export const setLanguageInfos = (endpoint, state) => {
    console.log('SOURCE')
    if(endpoint['translate_api']['id'] !== null) {
        if(endpoint['translate_api']['key'] == 'en') {
          state.isInIta=false
          state.isInEng=true
          if(endpoint['is_home']) {
                state.relativePathEng='/en';
          } else {
                state.relativePathEng=endpoint['translate_api']['path']
          }
        } else {
          state.isInIta=true;
          state.isInEng=false
          state.relativePathIta=endpoint['translate_api']['path']
          if(endpoint['is_home']) {
            state.relativePathIta='/'
            } else {
                state.relativePathIta=endpoint['translate_api']['path']
            }
        }
    } else {
        state.isInIta=false
        state.isInEng=false
    }
}