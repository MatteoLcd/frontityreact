
import React from "react"
import { connect } from "frontity"
import Switch from "@frontity/components/switch"

//Components
import PostTypeArchive from "./archive/PostTypeArchive"
import TaxonomyArchive from "./archive/TaxonomyArchive"

const IsArchive = ({ state, actions }) => {
  const data = state.source.get(state.router.link)

  return (
    <div>
      <Switch>
        <PostTypeArchive when={data.isPostTypeArchive}/>
        <TaxonomyArchive when={data.isTaxonomy}/>
      </Switch>
    </div>
  )
}

export default connect(IsArchive)