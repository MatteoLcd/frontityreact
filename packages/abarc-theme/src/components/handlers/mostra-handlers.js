const mostreHandler = {
    name:'mostre',
    priority:10,
    pattern: "/mostre/:slug",

    func: async ({link, state, params, libraries}) => {
      const mostra = state.source.data[link];
      
      Object.assign(mostra, {
        isSingle:true,
        isMostre: true,
        isPostType: true
      });
    }
  };

  export default mostreHandler;