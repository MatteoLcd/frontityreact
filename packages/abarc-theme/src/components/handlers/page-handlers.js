const postHandler = {
    pattern: "/bacheca/",
    func: async ({ route, params, state, libraries }) => {
      const response = await libraries.source.api.get({
        endpoint: "/wp/v2/posts",
        params: {
            per_page: 100,
            _embed: true
          }
      },);
      
  
      // You can name this as `items`... .
      const items = await libraries.source.populate({response, state , params });

      Object.assign(state.source.data[route], {
        isRandom: false,
        isMenu: true,
        // ...and use it directly here without using `map()`.
        items
      });
      
    }
  };
export default postHandler;