import { css } from 'frontity'

export const Single = () => {
    return(
        css`
            .single {
                &.single-mostre {
                    .entry-header {
                        margin-top: 72px;
                        @media screen and (min-width:1140px) {
                            margin-top: 144px;
                        }
                    }
                }
            }

            .m900-container {
                &-big {
                    padding:0 8px;
                    @media screen and (min-width:1140px) {
                        padding: 0 10px;
                    }
                }
                &-medium {
                    padding:0 16px;
                    @media screen and (min-width:600px) {
                        padding:0 24px;
                    }
                    @media screen and (min-width:1140px) {
                        padding: 0 50px;
                    }
                }
            }
        `
    )
}
