import { css } from 'frontity'
import { childCol, pxToRem } from '../styleFunctions/mixin'

export const Headings = () => {
    return(
        css`
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                clear: both;
                // font-family: $museoSans;
                font-weight: normal;
                font-style: normal;
                margin-bottom: 0;
                font-weight: 400;
                margin-top: 0;
                &.bold,
                &.is-style-bold {
                    font-weight: bold;
                    font-style: normal;
                }
                &.medium,
                &.is-style-medium {
                    font-weight: 500;
                    font-style: normal;
                }
            }



            .entry-content {
                h1,
                h2,
                h3,
                h4 {
                    margin-bottom: 32px;
                }
                
                h5,
                h6 {
                    margin-bottom: 24px;
                }
            }

            h1,
            .txt-h1,
            .is-style-txt-h1 {
                letter-spacing: 0;
                font-size:${pxToRem(40,16)};
                line-height: 40px;
                max-width: 900px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1140px) {
                    font-size:${pxToRem(72,16)};
                    line-height: 72px;
                    max-width: 1600px;
                    letter-spacing: -1px;
                }
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(90,16)};
                    line-height: 90px;
                    max-width: 2000px;
                }
            }

            .is-style-txt-h1 {
                &-medium {
                    letter-spacing: 0;
                    font-weight: 500;
                    font-style: normal;
                    font-size:${pxToRem(40,16)};
                    line-height: 40px;
                    max-width: 900px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(72,16)};
                        line-height: 72px;
                        max-width: 1600px;
                        letter-spacing: -1px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(90,16)};
                        line-height: 90px;
                        max-width: 2000px;
                    }
                }
                &-bold {
                    letter-spacing: 0;
                    font-weight: bold;
                    font-style: normal;
                    font-size:${pxToRem(40,16)};
                    line-height: 40px;
                    max-width: 900px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(72,16)};
                        line-height: 72px;
                        max-width: 1600px;
                        letter-spacing: -1px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(90,16)};;
                        line-height: 90px;
                        max-width: 2000px;
                    }
                }
            }

            h2,
            .txt-h2,
            .is-style-txt-h2 {
                letter-spacing: 0;
                font-size:${pxToRem(32,16)};
                line-height: 34px;
                max-width: 700px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1140px) {
                    font-size:${pxToRem(46,16)};
                    line-height: 46px;
                    max-width: 1000px;
                    letter-spacing: -0.2px;
                }
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(56,16)};
                    line-height: 56px;
                    letter-spacing: -0.5px;
                    max-width: 1200px;
                }
            }

            .is-style-txt-h2 {
                &-medium {
                    font-weight: 500;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(32,16)};
                    line-height: 34px;
                    max-width: 700px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(46,16)};
                        line-height: 46px;
                        max-width: 1000px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(56,16)};
                        line-height: 56px;
                        letter-spacing: -0.5px;
                        max-width: 1200px;
                    }
                }
                &-bold {
                    font-weight: bold;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(32,16)};
                    line-height: 34px;
                    max-width: 700px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(46,16)};
                        line-height: 46px;
                        max-width: 1000px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(56,16)};
                        line-height: 56px;
                        letter-spacing: -0.5px;
                        max-width: 1200px;
                    }
                }
            }

            h3,
            .txt-h3,
            .is-style-txt-h3 {
                letter-spacing: 0;
                font-size:${pxToRem(27,16)};
                line-height: 29px;
                max-width: 700px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1140px) {
                    font-size:${pxToRem(32,16)};
                    line-height: 34px;
                    letter-spacing: -0.2px;
                    max-width: 1000px
                }
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(40,16)};
                    line-height: 42px;
                    max-width: 1200px;
                }
            }

            .is-style-txt-h3 {
                &-medium {
                    font-size:${pxToRem(27,16)};
                    line-height: 29px;
                    font-weight: 500;
                    font-style: normal;
                    letter-spacing: 0;
                    max-width: 700px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(32,16)};
                        line-height: 34px;
                        letter-spacing: -0.2px;
                        max-width: 1000px
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(40,16)};
                        line-height: 42px;
                        max-width: 1200px;
                    }
                }
                &-bold {
                    font-weight: bold;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(27,16)};
                    line-height: 29px;
                    max-width: 700px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(32,16)};
                        line-height: 34px;
                        max-width: 1000px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(40,16)};
                        line-height: 42px;
                        max-width: 1200px;
                    }
                }
            }


            h4,
            .txt-h4,
            .is-style-txt-h4 {
                letter-spacing: 0;
                font-size:${pxToRem(22,16)};
                line-height: 24px;
                max-width: 550px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1140px) {
                    font-size:${pxToRem(24,16)};
                    line-height: 26px;
                    letter-spacing: -0.2px;
                }
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(32,16)};
                    line-height: 34px;
                    max-width: 650px;
                }
            }

            .is-style-txt-h4 {
                &-medium {
                    font-weight: 500;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(22,16)};
                    line-height: 24px;
                    max-width: 550px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(24,16)};
                        line-height: 26px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(32,16)};
                        line-height: 34px;
                        max-width: 650px;
                    }
                }
                &-bold {
                    font-weight: bold;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(22,16)};
                    line-height: 24px;
                    max-width: 550px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(24,16)};
                        line-height: 26px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(32,16)};
                        line-height: 34px;
                        max-width: 650px;
                    }
                }
            }



            h5,
            .txt-h5,
            .is-style-txt-h5 {
                font-size:${pxToRem(19,16)};
                line-height: 22px;
                letter-spacing: 0;
                max-width: 550px;
                letter-spacing: -0.1px;
                @media screen and (min-width:1140px) {
                    font-size:${pxToRem(20,16)};
                    line-height: 23px;
                    letter-spacing: -0.2px;
                }
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(24,16)};
                    line-height: 26px;
                    max-width: 650px;
                }
            }

            .is-style-txt-h5 {
                &-medium {
                    font-weight: 500;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(19,16)};
                    line-height: 22px;
                    max-width: 550px;
                    letter-spacing: -0.1px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(20,16)};
                        line-height: 23px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(24,16)};
                        line-height: 26px;
                        max-width: 650px;
                    }
                }
                &-bold {
                    font-weight: bold;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(19,16)};
                    line-height: 22px;
                    max-width: 550px;
                    letter-spacing: -0.1px;
                    @media screen and (min-width:1140px) {
                        font-size:${pxToRem(20,16)};
                        line-height: 23px;
                        letter-spacing: -0.2px;
                    }
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(24,16)};
                        line-height: 26px;
                        max-width: 650px;
                    }
                }
            }


            h6,
            .txt-h6,
            .is-style-txt-h6 {
                letter-spacing: 0;
                font-size:${pxToRem(18,16)};
                line-height: 21px;
                max-width: 550px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(21,16)};
                    line-height: 23px;
                    max-width: 650px;
                }
            }

            .is-style-txt-h6 {
                &-medium {
                    font-weight: 500;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(18,16)};
                    line-height: 21px;
                    max-width: 550px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(21,16)};
                        line-height: 23px;
                        max-width: 650px;
                    }
                }
                &-bold {
                    font-weight: bold;
                    font-style: normal;
                    letter-spacing: 0;
                    font-size:${pxToRem(18,16)};
                    line-height: 21px;
                    max-width: 550px;
                    letter-spacing: -0.2px;
                    @media screen and (min-width:1600px) {
                        font-size:${pxToRem(21,16)};
                        line-height: 23px;
                        max-width: 650px;
                    }
                }
            }


            .txt-logo {
                transition:0.2s ease-in-out;
                font-size:${pxToRem(36,16)};
                line-height: 30px;
                letter-spacing: -0.2px;
                @media screen and (min-width:1280px) {
                    font-size:${pxToRem(56,16)};
                    line-height: 50px;
                }
                &.is-scroll,
                &.is-open-like-scroll {
                    font-size:${pxToRem(25,16)};
                    line-height: 25px;
                    @media screen and (min-width:1280px) {
                        font-size:${pxToRem(56,16)};
                        line-height: 50px;
                    }
                }
            } 
        `
    )
}

