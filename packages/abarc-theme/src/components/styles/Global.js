import { css } from 'frontity'

export const GlobalTheme = () => {
    return(
        css`
            :root {
                --black:#000000;
                --primaryCyan:#62E1F0;
                --darkCyan:#16AFBD;
                --lightCyan:#9BFFFF;
                --white:#ffffff;
                --primaryGray:#67777F;
                --mediumGray:#9EB5C1;
                --darkGray:#263238;
                --lightGray:#ECEFF1;
                --md:1140;
                --sm:600;
            }
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Inter', sans-serif;
                &.roboto {
                    font-family: 'Roboto Slab', serif;
                }
            }
            html {
                font-family: system-ui, Verdana, Arial, sans-serif;
            }
        `
    )
}