import { css } from 'frontity'
import { childCol, pxToRem } from '../styleFunctions/mixin'

export const Copy = () => {
    return(
        css`
            .regular {
                font-family: $satoshi;
                font-weight: normal;
                font-style: normal;
            }

            p ,
            .txt-p {
                font-size:${pxToRem(16,16)};
                line-height: 19px;
                margin-top: 0;
                letter-spacing: 0.2px;
                font-weight: normal;
                font-style: normal;
                max-width: 550px;
                @media screen and (min-width:1600px) {
                    font-size:${pxToRem(20,16)};
                    line-height: 23px;
                    max-width: 650px;
                    letter-spacing: -0.2px;
                }
                &.bold,
                &.is-style-bold {
                    font-weight: bold;
                    font-style: normal;
                }
                &.medium,
                &.is-style-medium {
                    font-weight: 500;
                    font-style: normal;
                }
                &.cta {
                    margin-bottom: 0;
                }
                &.caption {
                    font-size:${pxToRem(12,16)};
                    line-height: 14px;
                    letter-spacing: 0.5px;
                }
            }

            .entry-content {
                p,
                .txt-p {
                    margin-bottom: 24px;
                }
            }

            dfn,
            cite,
            em,
            i {
                font-style: italic;
            }



            big {
                font-size: 125%;
            }

            .break-word {
                word-break: break-word;
            }

            .noLink {
                text-decoration: none;
                &:hover {
                    text-decoration: underline;
                    .hoverText {
                        text-decoration: underline!important;
                    }
                }
                &.noUnderline {
                    text-decoration: none!important;
                    * {
                        text-decoration: none!important; 
                    }
                }
                &.noHoverLink {
                    &:hover {
                        text-decoration: none;
                    }
                }
            }

            .noOutline {
                outline: none!important;
            }

            .uppercase {
                text-transform: uppercase;
            }
        `
    )
}


