import { css } from 'frontity'
export const Display = () => {
    return(
        css`
            .d-flex {
                display: flex;
            }
        `
    )
}