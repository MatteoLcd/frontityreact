import { css } from 'frontity'
import { childCol } from '../../styleFunctions/mixin'

export const Visita = () => {
    return(
        css`
            .wp-block-m900-blocks-informazioni-visita {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                &.m900-container-medium {
                    padding-top: 48px;
                    padding-bottom: 48px;
                    border-bottom: solid 1px var(--mediumGray);
                    @media screen and (min-width:1140px) {
                        padding-bottom: 72px;
                        padding-top: 72px;
                        margin-top: 0;
                        margin-bottom: 0;
                    }
                }
                .informazioni-visita-content {
                    @media screen and (min-width:1140px) {
                        position: sticky;
                        top:160px;
                    }
                    .titolo-visita {
                        margin-bottom: 24px;
                        @media screen and (min-width:600px) {
                            margin-bottom: 32px;
                            width:${childCol(8, 6)};
                        }
                        @media screen and (min-width:1140px) {
                            margin-bottom: 48px;
                            width: 100%;
                        }
                    }
                    .descrizione-visita {
                        margin-bottom: 0;
                        @media screen and (min-width:600px) {
                            width:${childCol(8, 6)};
                        }
                        @media screen and (min-width:1140px) {
                            width: 100%;
                        }
                    }
                }
                .informazioni-visita {
                    &--col-sx {
                        width: 100%;
                        @media screen and (min-width:1140px) {
                            width:${childCol(12, 5)};
                            padding-right: 12px;
                        }
                    }
                    &--col-dx {
                        margin-top: 48px;
                        width: $col12;
                    @media screen and (min-width:1140px) {
                        margin-top: 0;
                        width:${childCol(12, 6)};
                        padding-left: 10px;
                    }
                    .wp-block-m900-blocks-inner-box-visita {
                        &:nth-last-of-type(1) {
                            margin-bottom: 0;
                        }
                        .inner-box-title {
                            margin-bottom: 24px;
                            @media screen and (min-width:1140px) {
                                margin-bottom: 48px;
                            }
                        }
                    }
                    }
                .descrizione-visita {
                    @media screen and (min-width:1140px) {
                        margin-top: 48px;
                    }
                }
                }
                .nota-visita {
                    margin-bottom: 0;
                    margin-top: 32px;
                    @media screen and (min-width:600px) {
                        width:${childCol(8, 6)};
                    }
                    @media screen and (min-width:1140px) {
                        margin-top: 48px;
                        width:${childCol(5, 4)};
                        padding-right: 4px;
                    }
                }
            }
        `
    )
}


