import React from "react";
import { connect, styled } from "frontity";


const LanguageSwitch = ({ actions, state }) => {
    const { lang } = state.theme;
    console.log('LANG',lang)
    return (
        <>
            <Box>      
                {state.theme.isInEng && 
                    <a
                        href={state.theme.relativePathEng !== '' ? `${state.theme.relativePathEng}` : '/en/'}
                        className="toggleicon"  
                        onClick={actions.theme.toggleLang}
                    >EN</a>
                }
                {state.theme.isInIta && 
                    <a 
                    // href="/" 
                    href={state.theme.relativePathIta !== '' ? `${state.theme.relativePathIta}` : '/'}
                    className="toggleicon">
                        IT
                    </a>
                }
            </Box>
        </>
    )
}

export default connect(LanguageSwitch);

const Box = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 1rem;
    cursor: pointer;
    z-index: 5;

    img {
        height: 25px;
        width: 25px;
    }
`;

