import React from 'react'
import { Swiper, SwiperSlide } from "swiper/react";

const Gallery = ({slides}) => {
  // console.log('GALLERY')
  return (
      <>
      <Swiper slidesPerView={3} spaceBetween={30} className="mySwiper">
          {slides.map(function(item, i){
            // console.log(item.children[0].props.src);
            const imgUrl = item.children[0].props.src;
            return (
              <SwiperSlide key={i}>
                <img src={imgUrl}/>
              </SwiperSlide>
            )
            })
          }
      </Swiper>
      </>
  )
}

const gallery = {
  name: 'gallery',
  priority: 20,
  test: ({ props }) => props.className === "swiper-wrapper",
  processor: ({ node }) => {
    // console.log(node.children)
      const slides = node.children;
           
      return {
        component: Gallery,
        props: { slides },
      }
    },
}

export default gallery;