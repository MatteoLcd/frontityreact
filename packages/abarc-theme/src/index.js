import Root from "./components"
import link from "@frontity/html2react/processors/link";

//handlers
import menuHandler from "./components/handlers/menu-handler";
import postHandler from './components/handlers/page-handlers'
import mostreHandler from './components/handlers/mostra-handlers'

//Processor
import quote from "./processors/quote";

const abarcTheme = {
  name: "abarc-theme",
  roots: {
    theme: Root,
  },
  state: {
    theme: {
      isUrlVisible:false,
      menu: [],
      lang: "it",
      rapidNavIta:'menu-ita',
      rapidNavEng:'menu-eng',
      relativePathEng:'',
      relativePathIta:'',
      isInEng:false,
      isInIta:false,
    },
  },
  actions: {
    theme: {
      toggleLang:({state}) => {
        console.log('LANG',state.lang),
        state.theme.lang = state.lang
      },
      toggleUrl:({state}) => {
        state.theme.isUrlVisible = !state.theme.isUrlVisible
      },
      beforeSSR: async ({ state, actions }) => {
        // console.log('BEFORE SSR')
        // await actions.source.fetch(`/menu/${state.theme.menuPrincipale}/`);
        // await actions.source.fetch(`/menu/${state.theme.menuFooter}/`);
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/pages`);
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/posts`)
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/eventi`);
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/tipodievento`);
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/mostre`);
        // await actions.source.fetch("/category");

        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/menu/${state.theme.rapidNavIta}`);
        await actions.source.fetch(`${state.lang === 'en' ? '/en' : ''}/menu/${state.theme.rapidNavEng}`);
        // await actions.source.fetch(`/bacheca/`);
        // await actions.source.fetch("/widgets");
      },
    },
  },
  libraries: {
    html2react: {
      processors: [link]
    },
    source: {
      handlers: [menuHandler, mostreHandler],
    },
  }
}


export default abarcTheme