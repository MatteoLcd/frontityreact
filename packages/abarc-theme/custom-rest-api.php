<?php
//FUNZIONE PER REGISTRARE GLI ID RELATIVI ALLA LINGUA

add_action( 'rest_api_init', function () {
	register_rest_field( 'post', 'translate_api', mol_related_translation_api() );
    register_rest_field( 'page', 'translate_api', mol_related_translation_api() );
	register_rest_field( 'mostre', 'translate_api', mol_related_translation_api() ); // Optional: Custom posttype
	register_rest_field( 'eventi', 'translate_api', mol_related_translation_api() ); // Optional: Custom posttype
	register_rest_field( 'collezioni', 'translate_api', mol_related_translation_api() ); // Optional: Custom posttype
	register_rest_field( 'persone', 'translate_api', mol_related_translation_api() ); // Optional: Custom posttype
	register_rest_field( 'press', 'translate_api', mol_related_translation_api() ); // Optional: Custom posttype
});

function mol_related_translation_api() {
	return array(
		'methods'         => 'GET',
		'get_callback'    => 'related_translate_api',
		'schema'          => null,
	);
}

function related_translate_api( $data ) {
	$post_id = $data['id'];
    $currentLang = apply_filters( 'wpml_post_language_details', '', $post_id )['language_code'];
    if($currentLang === 'it') {
        $otherLang = 'en';
    } else {
        $otherLang = 'it';
    }

    $postData = apply_filters( 'wpml_object_id', $post_id, get_post_type(), false, $otherLang );
    $langArray = array(
        'key'   => apply_filters( 'wpml_post_language_details', '', $postData )['language_code'],
        'id'    => $postData,
        'slug'  => get_post($postData)->post_name,
        'path'  => wp_make_link_relative(get_the_permalink($postData))
    );

    return $langArray;
}


/// PAGINA PER POSTS

add_action( 'rest_api_init', function () {
    register_rest_field( 'page', 'is_home', mol_is_site_home_page() );
});

function mol_is_site_home_page() {
	return array(
		'methods'         => 'GET',
		'get_callback'    => 'register_is_site_home_api_rest',
		'schema'          => null,
	);
}

function register_is_site_home_api_rest( $data ) {
	$post_id = $data['id'];
    
    if ( intval(get_option( 'page_on_front' )) === $post_id ) {
        $PPPosts = true;
    } else {
        $PPPosts = false;
    }

    
    return $PPPosts;
}



/// CATEGORIE

add_action( 'rest_api_init', function () {
    register_rest_field( 'category', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'tipodimostra', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'tipodicollezione', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'artisti', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'tecnica', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'tipodievento', 'taxonomy_translate', mol_test_category() );
    register_rest_field( 'ruolo', 'taxonomy_translate', mol_test_category() );
});

function mol_test_category() {
	return array(
		'methods'         => 'GET',
		'get_callback'    => 'register_test_category',
		'schema'          => null,
	);
}

function register_test_category( $data ) {
	$term_id = $data['id'];


    $term = get_term( $term_id );
    $taxonomy = $term->taxonomy;

    $currentLang = apply_filters( 'wpml_current_language', null );
    if($currentLang === 'it') {
        $otherLang = 'en';
    } else {
        $otherLang = 'it';
    }

    global $translateId;
    $translateId = apply_filters( 'wpml_object_id', $term_id, $taxonomy, true , $otherLang );
    

    $translated_url = apply_filters( 'wpml_permalink', get_term_link($term_id) , $otherLang, true);
    $slugParts = explode("/", $translated_url);

    if($translateId !== $term_id) {
        $langArray = array(
            'key'   => $otherLang,
            'id'    => $translateId,
            'slug'  => $slugParts[count($slugParts) - 2],
            'path'  => wp_make_link_relative($translated_url)
        );
    } else {
        $langArray = array(
            'key'   => null,
            'id'    => null,
            'slug'  => null,
            'path'  => null
        );
    }

    
    return $langArray;
}


?>