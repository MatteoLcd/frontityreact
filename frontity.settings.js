const settings = [
  {
    name: "frontity-react-ita",
    state: {
      frontity: {
        url: "http://m900react.local/",
        title: "Test Frontity Blog",
        description: "WordPress installation for Frontity development"
      }
    },
    packages: [
      {
        name: "abarc-theme",
        state: {
          lang: "it",
          theme: {
            menu: [],
            featured: {
              showOnList: false,
              showOnPost: false
            }
          }
        }
      },
      {
        name: "@frontity/wp-source",
        state: {
          source: {
            url: "http://m900react.local/",
            api: "http://m900react.local/wp-json",
            homepage: "/home-page",
            postsPage: "/news",
            params: {
              lang: "it",
            },
            postTypes:[
              {
                type: "mostre",
                endpoint: "mostre",
                archive: "/mostre"
              },
              {
                type: "collezioni",
                endpoint: "collezioni",
                archive: "/collezioni"
              },
              {
                type: "eventi",
                endpoint: "eventi",
                archive: "/eventi"
              },
              {
                type: "persone",
                endpoint: "persone",
                archive: "/persone"
              },
              {
                type: "press",
                endpoint: "press",
                archive: "/press"
              },
            ],
            taxonomies:[
              // {
              //   taxonomy:"category",
              //   endpoint:"categories",
              //   postTypeEndpoint:'post',
              //   params:{
              //     per_page:5,
              //     _embed:true
              //   }
              // },
              {
                taxonomy:"tipodimostra",
                endpoint:"tipodimostra",
                postTypeEndpoint:'mostre',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tipodicollezione",
                endpoint:"tipodicollezione",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"artisti",
                endpoint:"artisti",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tecnica",
                endpoint:"tecnica",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"annoopera",
                endpoint:"annoopera",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tipodievento",
                endpoint:"tipodievento",
                postTypeEndpoint:'eventi',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"ruolo",
                endpoint:"ruolo",
                postTypeEndpoint:'/persone',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
            ]
          }
        }
      },
      "@frontity/tiny-router",
      "@frontity/html2react"
    ]
  },
  {
    name: "frontity-react-eng",
    // match: [".*localhost:3000\/en(\/.*)?$"],
    match: [".*localhost:3000/en(\/.*)?$"],
    state: {
      frontity: {
        url: "http://m900react.local/",
        title: "Test Frontity Blog",
        description: "WordPress installation for Frontity development"
      }
    },
    packages: [
      {
        name: "abarc-theme",
        state: {
          lang: "en",
          theme: {
            menu: [],
            featured: {
              showOnList: false,
              showOnPost: false
            }
          }
        }
      },
      {
        name: "@frontity/wp-source",
        state: {
          source: {
            url: "http://m900react.local/en",
            api: "http://m900react.local/en/wp-json",
            homepage: "/home-page",
            postsPage: "/news",
            subdirectory: "/en/",
            params: {
              "lang": "en",
            },
            postTypes:[
              {
                type: "mostre",
                endpoint: "mostre",
                archive: "/mostre"
              },
              {
                type: "collezioni",
                endpoint: "collezioni",
                archive: "/collezioni"
              },
              {
                type: "eventi",
                endpoint: "eventi",
                archive: "/eventi"
              },
              {
                type: "persone",
                endpoint: "persone",
                archive: "/persone"
              },
              {
                type: "press",
                endpoint: "press",
                archive: "/press"
              },
            ],
            taxonomies:[
              // {
              //   taxonomy:"category",
              //   endpoint:"categories",
              //   postTypeEndpoint:'news',
              //   params:{
              //     per_page:5,
              //     _embed:true
              //   }
              // },
              {
                taxonomy:"tipodimostra",
                endpoint:"tipodimostra",
                postTypeEndpoint:'mostre',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tipodicollezione",
                endpoint:"tipodicollezione",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"artisti",
                endpoint:"artisti",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tecnica",
                endpoint:"tecnica",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"annoopera",
                endpoint:"annoopera",
                postTypeEndpoint:'collezioni',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"tipodievento",
                endpoint:"tipodievento",
                postTypeEndpoint:'eventi',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
              {
                taxonomy:"ruolo",
                endpoint:"ruolo",
                postTypeEndpoint:'persone',
                params:{
                  per_page:5,
                  _embed:true
                }
              },
            ]
          }
        }
      },
      "@frontity/tiny-router",
      "@frontity/html2react"
    ]
  }
  ]
  export default settings;

// const settings = 
// [
//   {
//     "name": "frontity-react-ita",
//     "match": [".*localhost:3000\/it(\/.*)?$"],
//     "state": {
//       "frontity": {
//         "url": "http://abarcalreact.local/",
//         "title": "Test Frontity Blog",
//         "description": "WordPress installation for Frontity development"
//       },
//       "theme": {
//         "menuUrl": "menu",
//         "lang": "it"
//       }
//     },
//     "packages": [
//       {
//         name: "abarc-theme",
//         state: {
//           lang:'it',
//           theme: {
//             menu:[]
//           }
//         }
//       },
//       {
//         "name": "@frontity/wp-source",
//         "state": {
//           "source": {
//             "url": "http://abarcalreact.local",
//             "homepage": "/",
//             "postsPage": "bacheca",
//             "params": {
//               "lang": "it",
//             },
//             "postTypes":[
//               {
//                 type: "eventi",
//                 endpoint: "eventi",
//                 archive: "/eventi"
//               },
//               {
//                 type: "collezioni",
//                 endpoint: "collezioni",
//                 archive: "/collezioni"
//               },
//               {
//                 type: "didattica",
//                 endpoint: "didattica",
//                 archive: "/didattica"
//               },
//               {
//                 type: "materie",
//                 endpoint: "materie",
//                 archive: "/materie"
//               },
//               {
//                 type: "docenti",
//                 endpoint: "docenti",
//                 archive: "/docenti"
//               },
//             ]
//           }
//         }
//       },
//       "@frontity/tiny-router",
//       "@frontity/html2react"
//     ]
//   },
//   {
//     "name": "frontity-react-en",
//     "match": [".*localhost:3000\/en(\/.*)?$"],
//     "state": {
//       "frontity": {
//         "url": "http://abarcalreact.local/en",
//         "title": "Test Frontity Blog",
//         "description": "WordPress installation for Frontity development"
//       },
//       "theme": {
//         "menuUrl": "menu",
//         "lang": "en"
//       }
//     },
//     "packages": [
//       {
//         name: "abarc-theme",
//         state: {
//           lang:'en',
//           theme: {
//             menu:[]
//           }
//         }
//       },
//       {
//         "name": "@frontity/wp-source",
//         "state": {
//           "source": {
//             "url": "http://abarcalreact.local",
//             "homepage": "/home",
//             "postsPage": "/en/news",
//             "params": {
//               "lang": "en",
//             },
//             "postTypes":[
//               {
//                 type: "eventi",
//                 endpoint: "eventi",
//                 archive: "/eventi"
//               },
//               {
//                 type: "collezioni",
//                 endpoint: "collezioni",
//                 archive: "/collezioni"
//               },
//               {
//                 type: "didattica",
//                 endpoint: "didattica",
//                 archive: "/didattica"
//               },
//               {
//                 type: "materie",
//                 endpoint: "materie",
//                 archive: "/materie"
//               },
//               {
//                 type: "docenti",
//                 endpoint: "docenti",
//                 archive: "/docenti"
//               },
//             ]
//           }
//         }
//       },
//       "@frontity/tiny-router",
//       "@frontity/html2react"
//     ]
//   },
// ]

// export default settings;
